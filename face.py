import cv2
import numpy as np
import dlib

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("C:/Users/alexa/Documents/Travail_Perso/Programmation/Python/OpenCV/Eye open CV/shape_predictor_68_face_landmarks.dat")

while True:
    _, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = detector(gray)
    for face in faces:
        x1 = face.left()
        y1 = face.top()
        x2 = face.right()
        y2 = face.bottom()
        x3 = x1
        y3 = y1 - 10
        cv2.putText(frame,"Visage",(x3,y3),cv2.FONT_HERSHEY_COMPLEX,0.5,(255,255,255),1)
        cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 255), 3)

        landmarks = predictor(gray, face)

        for n in range(0, 68):
            x = landmarks.part(n).x
            y = landmarks.part(n).y
            cv2.circle(frame, (x, y), 4, (0, 255, 0), -1)


    cv2.imshow("Face detector", frame)

    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()